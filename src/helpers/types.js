/**
 * @typedef {Object} Message
 * @property {User} user - L'utilisateur qui a envoyé le message.
 * @property {string} text - Le contenu du message.
 * @property {number} timestamp - La date et l'heure à laquelle le message a été envoyé (en millisecondes).
 *
 * @property getFormattedDate - Retourne la date et l'heure formatées du message.
 */

/**
 * @typedef {Object} Command
 * @property {string} triggerText - Le texte qui déclenche l'exécution de la commande.
 * @property {async (rest: string, mainUser: User) => Message} action - La fonction qui sera exécutée lors de l'exécution de la commande.
 */

/**
 * @typedef {Object} User
 * @property {number} id - Identifiant unique de l'utilisateur.
 * @property {string} name - Nom de l'utilisateur.
 * @property {string} avatar - URL de l'image d'avatar de l'utilisateur.
 * @property {Array<Command>} commands - Liste des commandes du robot.
 */

const Types = {};

export default Types;
