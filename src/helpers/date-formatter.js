export default class DateFormatter {
    /**
     * Formate la date et l'heure du message.
     *
     * @param {number} timestamp Le timestamp à formater.
     *
     * @returns {string} La date et l'heure formatées.
     */
    static format(timestamp) {
        const date = new Date(timestamp);
        const frenchMonths = [
            'Janvier',
            'Février',
            'Mars',
            'Avril',
            'Mai',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre',
        ];

        // Obtient les éléments de la date actuelle
        const jour = date.getDate();
        const mois = frenchMonths[date.getMonth()];
        const annee = date.getFullYear();
        const heure = date.getHours();
        const minute = date.getMinutes();

        // Formate la date en chaîne de caractères
        return `${jour} ${mois} ${annee} à ${heure}:${minute}`;
    }
}
