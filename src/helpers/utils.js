import ChatGPT from '../apis/chat-gpt.js';
import Message from '../models/message.js';
import * as Types from './types.js';

export default class Utils {
    /**
     * Ajoute les utilisateurs dans le DOM.
     *
     * @param {Element} usersContainer l'élément HTML qui contient tous les utilisateurs.
     * @param {Array<Types.User>} users liste des utilisateurs à afficher.
     *
     * @returns {string}
     */
    static addUsersToDOM(usersContainer, users) {
        // Génère le code HTML pour chaque utilisateur
        const htmlUsers = users.map((user) => {
            if (user.isMainUser) {
                return `
                    <div class="flex items-center space-x-4">
                        <img
                            class="w-10 h-10 rounded-full"
                            src="${user.avatar}"
                            alt="${user.name}"
                        />
                        <div class="font-medium">
                            <div>${user.name}</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Utilisateur</div>
                        </div>
                    </div>
                `;
            }

            return `
                <div class="flex items-center space-x-4">
                    <img
                        class="w-10 h-10 rounded-full"
                        src="${user.avatar}"
                        alt="${user.name}"
                    />
                    <div class="font-medium">
                        <div>${user.name}</div>
                        <div class="text-sm text-gray-500 dark:text-gray-400">Robot</div>
                    </div>
                </div>
            `;
        });

        // Ajoute les utilisateurs au DOM
        usersContainer.insertAdjacentHTML('beforeend', htmlUsers.join(''));
    }

    /**
     * Génère le code HTML pour un message.
     *
     * @param {Types.Message} message le message à afficher.
     *
     * @returns {string} le code HTML généré.
     */
    static generateHTMLMessage(message) {
        if (message.user.isMainUser) {
            // HTML pour le message de l'utilisateur principal
            return `
                <div class="flex items-end justify-end">
                    <div class="flex flex-col mx-2 order-1 items-end">
                        <div class="flex flex-col px-4 py-2 rounded-lg rounded-br-none bg-orange-400 w-56">
                            <p class="font-bold text-sm text-orange-900">${message.user.name}</p>
                            <p class="text-sm pb-1 text-orange-50">${message.text}</p>
                            <span class="text-orange-100 text-xs self-end pt-2">
                                ${message.getFormattedDate()}
                            </span>
                        </div>
                    </div>
                    <img
                        src="${message.user.avatar}"
                        alt="${message.user.name}"
                        title="${message.user.name}"
                        class="w-6 h-6 rounded-full order-2"
                    />
                </div>
            `;
        }

        // HTML pour les messages des autres utilisateurs
        return `
            <div class="flex items-end">
                <div class="flex flex-col mx-2 order-2 items-start">
                    <div class="flex flex-col px-4 py-2 rounded-lg rounded-bl-none bg-gray-300">
                        <p class="font-bold text-sm text-gray-500">${message.user.name}</p>
                        <p class="text-sm pb-1 text-gray-600">
                            ${message.text}
                        </p>
                        <span class="text-gray-400 text-xs self-end pt-2">
                            ${message.getFormattedDate()}
                        </span>
                    </div>
                </div>
                 <img
                    src="${message.user.avatar}"
                    alt="${message.user.name}"
                    class="w-6 h-6 rounded-full order-1"
                />
            </div>
        `;
    }

    /**
     * Ajoute un message au DOM.
     *
     * @param {Element} messagesContainer l'élément HTML qui contient tous les messages.
     * @param {Types.Message} message le message à ajouter au DOM.
     */
    static addMessageToDOM(messagesContainer, message) {
        // Génère le code HTML du message
        const htmlMessage = this.generateHTMLMessage(message);
        // Ajoute le message au DOM
        messagesContainer.insertAdjacentHTML('beforeend', htmlMessage);
        // Scroll vers le bas pour voir le dernier message
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
    }

    /**
     * Sauvegarde les messages dans le localStorage.
     *
     * @param {Array<Types.Message>} messages
     */
    static registerLocalStorageMessages(messages) {
        // Save messages in localStorage
        localStorage.setItem('messages', JSON.stringify(messages));
    }

    /**
     * Ajoute un nouveau message à la liste des messages existants et sauvegarde les messages dans le localStorage.
     *
     * @param {Array<Types.Message>} existingMessages la liste des messages existants.
     * @param {string} newMessage le texte du nouveau message.
     * @param {Types.User} user l'utilisateur qui a envoyé le nouveau message.
     *
     * @returns {Array<Types.Message>} la liste des messages existants avec le nouveau message ajouté.
     */
    static addNewUserMessage(existingMessages, newMessage, user) {
        // Ajoute un nouveau message à la liste des messages existants
        existingMessages.push(
            new Message({
                user: user,
                text: newMessage,
                timestamp: Date.now(),
            })
        );

        // Sauvegarde les messages dans le localStorage
        localStorage.setItem('messages', JSON.stringify(existingMessages));

        return existingMessages;
    }

    /**
     *
     * @param {Element} inputMessage l'élément HTML qui contient le texte du message.
     * @param {Types.User} mainUser l'utilisateur principal.
     * @param {Array<Types.User>} robots la liste des robots.
     *
     * @returns {Promise<Array<Types.Message>>} le message de l'utilisateur principal et les réponses des robots s'il y en a.
     */
    static async handleNewMessage(inputMessage, mainUser, robots) {
        // Récupérer la valeur de l'input
        const messageText = inputMessage.value;
        // Créer un nouveau message
        const message = new Message({
            user: mainUser,
            text: messageText,
            timestamp: Date.now(),
        });

        /**
         * Traiter le message et générer les réponses des robots si nécessaire
         *
         * @type {Array<Message>|undefined}
         */
        const robotsMessages = await Utils.processMessage(message, mainUser, robots);

        // Vider l'input
        inputMessage.value = '';

        return [message, ...(robotsMessages ?? [])];
    }

    /**
     * Traite un message et génère les réponses des robots si nécessaire.
     *
     * @param {Types.Message} message le message à traiter.
     * @param {Types.User} mainUser l'utilisateur principal.
     * @param {Array<Types.User>} robots la liste des robots.
     *
     * @returns {Promise<Array<Types.Message>|undefined>} les réponses des robots s'il y en a.
     */
    static async processMessage(message, mainUser, robots) {
        let messageText = message.text.toLocaleLowerCase();

        if (messageText.startsWith('!')) {
            // Supprime le point d'exclamation du début du message
            messageText = messageText.slice(1);

            // Commande d'aide
            if (messageText.startsWith('help')) {
                // Ajouter un message pour chaque robot avec leurs commandes
                return robots.map(
                    (robot) =>
                        new Message({
                            user: robot,
                            timestamp: Date.now(),
                            text: `
                                    <p>Je supporte les commandes suivantes :</p>
                                    <ul class="list-disc pl-6">
                                        <li>!bonjour - J'affiche un message de bienvenue</li>
                                        <li>!chatgpt "${robot.name}" <u>message</u> - Je réponds à votre message</li>
                                        ${robot.commands.map((command) => `<li>${command.description}</li>`).join('')}
                                    </ul>
                                `,
                        })
                );
            }

            // Commande ChatGPT
            if (messageText.startsWith('chatgpt')) {
                // Suppression de la commande dans le message
                let messageTextWithoutCommand = messageText.replace('chatgpt', '').trim();
                // Récupération du nom du robot depuis le message
                const robotName = messageTextWithoutCommand.match(/"([^"]+)"/)[1].trim();
                // Suppression du nom du robot dans le message
                messageTextWithoutCommand = messageTextWithoutCommand.replace(`"${robotName}" `, '').trim();
                // Récupérer le robot à partir de son nom
                const robot = robots.find((robot) => robotName.toLocaleLowerCase() === robot.name.toLocaleLowerCase());
                if (undefined !== robot) {
                    // Appel à ChatGPT pour générer une réponse
                    const chatGPTResponse = await ChatGPT.getResponse(messageTextWithoutCommand);

                    // Créer le message du robot à partir de la réponse de ChatGPT
                    return [
                        new Message({
                            user: robot,
                            text: chatGPTResponse,
                            timestamp: Date.now(),
                        }),
                    ];
                }
            }

            // Commande commune à tous les robots
            if (messageText.includes('bonjour')) {
                // Tous les robots disent bonjour à l'utilisateur principal
                return robots.map(
                    (robot) =>
                        new Message({
                            user: robot,
                            text: `Bonjour ${mainUser.name} ! Je suis ${robot.name}. Comment puis-je vous aider ?`,
                            timestamp: Date.now(),
                        })
                );
            }

            // Chercher la commande dans la liste des commandes des robots
            for (const robot of robots) {
                const robotCommand = robot.commands.find((command) => messageText.startsWith(command.triggerText));
                if (robotCommand) {
                    // Supprime le texte de la commande
                    const rest = messageText.replace(robotCommand.triggerText, '').trim();

                    return Array(await robotCommand.action(rest, mainUser));
                }
            }
        }
    }

    /**
     * Génère le code HTML du bouton d'envoi du message.
     *
     * @param {Element} buttonMessage l'élément HTML qui contient le bouton d'envoi du message.
     * @param {boolean} isLoading true si le bouton doit être en mode "chargement".
     *
     * @returns {string} le code HTML du bouton d'envoi du message.
     */
    static setSendButtonState(buttonMessage, isLoading) {
        if (isLoading) {
            // Ajouter l'attribut "disabled" au bouton
            buttonMessage.setAttribute('disabled', 'disabled');
            // Ajouter le code HTML du bouton en mode "chargement"
            buttonMessage.innerHTML = `
                <svg
                    aria-hidden="true"
                    role="status"
                    class="mr-3 inline h-4 w-4 animate-spin text-white"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                        fill="#E5E7EB"
                    />
                    <path
                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                        fill="currentColor"
                    />
                </svg>
                Envoi...
            `;
        } else {
            // Supprimer l'attribut "disabled" du bouton
            buttonMessage.removeAttribute('disabled');

            buttonMessage.innerHTML = `
                <span class="font-bold">Envoyer</span>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    class="ml-2 h-6 w-6 rotate-90 transform"
                >
                    <path
                        d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"
                    />
                </svg>
            `;
        }
    }
}
