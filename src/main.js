import './styles.css';

import Utils from './helpers/utils';
import Message from './models/message';
import Command from './models/command';
import User from './models/user';
import OpenWeatherMap from './apis/open-weather-map';
import Github from './apis/github';
import TheMovieDb from './apis/themoviedb';

// Importer les images
import ImageUser from '/src/assets/user.png';
import ImageRobot1 from '/src/assets/robot1.png';
import ImageRobot2 from '/src/assets/robot2.png';
import ImageRobot3 from '/src/assets/robot3.png';

/**
 * @typedef {import('./helpers/types').User} User
 * @typedef {import('./helpers/types').Message} Message
 * @typedef {import('./helpers/types').Command} Command
 */

// Créer l'utilisateur principal
const mainUser = new User({
    id: 1,
    name: 'John Doe',
    avatar: ImageUser,
    isMainUser: true,
});

// Créer les 3 robots
const robot1 = new User({
    id: 2,
    name: 'Robot 1',
    avatar: ImageRobot1,
    commands: [
        new Command({
            triggerText: 'meteo',
            description: '!meteo <u>ville</u> - Je recherche la météo de la ville donnée',
            action: async (rest) => {
                const weather = await OpenWeatherMap.getWeather(rest);
                return new Message({
                    user: robot1,
                    text: weather,
                    timestamp: Date.now(),
                });
            },
        }),
    ],
});
const robot2 = new User({
    id: 3,
    name: 'Robot 2',
    avatar: ImageRobot2,
    commands: [
        new Command({
            triggerText: 'github',
            description:
                "!github <u>nom d'utilisateur</u> - Rechercher un utilisateur Github par son nom d'utilisateur",
            action: async (rest) => {
                const userInfo = await Github.getUser(rest);
                return new Message({
                    user: robot2,
                    text: userInfo,
                    timestamp: Date.now(),
                });
            },
        }),
    ],
});
const robot3 = new User({
    id: 4,
    name: 'Robot 3',
    avatar: ImageRobot3,
    commands: [
        new Command({
            triggerText: 'movie',
            description: '!movie <u>nom du film</u> - Rechercher un film par son nom',
            action: async (rest) => {
                const movieInfo = await TheMovieDb.getMovie(rest);
                return new Message({
                    user: robot3,
                    text: movieInfo,
                    timestamp: Date.now(),
                });
            },
        }),
    ],
});

// Récupérer les éléments du DOM
const messagesContainer = document.querySelector('#messages');
const clearMessages = document.querySelector('#clearMessages');
const buttonMessage = document.querySelector('#buttonMessage');
const inputMessage = document.querySelector('#inputMessage');
const usersContainer = document.querySelector('#usersContainer');

// Ajouter les utilisateurs dans le DOM
Utils.addUsersToDOM(usersContainer, [mainUser, robot1, robot2, robot3]);

/**
 * Récupérer les messages depuis le localStorage
 *
 * @type {Array<Message>}
 */
let messages = (JSON.parse(localStorage.getItem('messages')) ?? []).map((message) => new Message(message));

// Ajouter les messages au DOM
messages.forEach((message) => Utils.addMessageToDOM(messagesContainer, message));

// Ajouter un écouteur d'événement sur la touche entrée
inputMessage.addEventListener('keyup', (event) => {
    if ('Enter' === event.key) {
        // Gérer le nouveau message
        handleNewMessage();
    } else if ('ArrowUp' === event.key && !inputMessage.value) {
        // Filtrer les messages de l'utilisateur principal
        const filteredMessages = messages.filter((message) => message.user.isMainUser);
        // Récupérer le dernier message
        const lastMessage = filteredMessages[filteredMessages.length - 1];
        if (lastMessage) {
            // Modifier la valeur de l'input
            inputMessage.value = lastMessage.text;
        }
    }
});

// Ajouter un écouteur d'événement sur le bouton d'envoi de message
buttonMessage.addEventListener('click', (event) => {
    // Empêcher l'action par défaut du bouton
    event.preventDefault();
    // Gérer le nouveau message
    handleNewMessage();
});

clearMessages.addEventListener('click', () => {
    messages = [];
    localStorage.clear();
    messagesContainer.innerHTML = '';
});

// Gérer le nouveau message
async function handleNewMessage() {
    if ('' !== inputMessage.value.trim()) {
        // Désactiver le bouton d'envoi de message
        Utils.setSendButtonState(buttonMessage, true);
        /**
         * Gérer le nouveau message
         *
         * @type {Array<Message>}
         */
        const newMessages = await Utils.handleNewMessage(inputMessage, mainUser, [robot1, robot2, robot3]);
        // Traiter les nouveaux messages
        newMessages.forEach((message) => {
            // Ajouter le message au DOM
            Utils.addMessageToDOM(messagesContainer, message);
            // Ajouter le message au tableau des messages
            messages.push(message);
        });
        // Enregistrer les messages dans le localStorage
        Utils.registerLocalStorageMessages(messages);
        // Activer le bouton d'envoi de message
        setTimeout(() => Utils.setSendButtonState(buttonMessage, false), 1000);
    }
}
