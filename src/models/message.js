import DateFormatter from '../helpers/date-formatter';

export default class Message {
    constructor(data) {
        this.user = data.user;
        this.text = data.text;
        this.timestamp = data.timestamp;
    }

    /**
     * Formate la date et l'heure du message.
     *
     * @returns {string} La date et l'heure formatées du message.
     */
    getFormattedDate() {
        return DateFormatter.format(this.timestamp);
    }
}
