export default class User {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.avatar = data.avatar;
        this.isMainUser = data.isMainUser ?? false;
        this.commands = data.commands ?? [];
    }
}
