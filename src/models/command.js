export default class Command {
    constructor(data) {
        this.triggerText = data.triggerText;
        this.action = data.action;
        this.description = data.description;
    }
}
