export default class TheMovieDb {
    /**
     * Recherche les informations d'un film.
     *
     * https://developers.themoviedb.org/3/search/search-movies
     *
     * @param {string} movie le nom du film à rechercher.
     *
     * @returns {Promise<string>} une phrase décrivant le film.
     */
    static async getMovie(movie) {
        const apiURL = new URL(import.meta.env.VITE_TMDB_MOVIE_ENTRYPOINT);
        apiURL.searchParams.set('api_key', import.meta.env.VITE_TMDB_APIKEY);
        apiURL.searchParams.set('query', movie);
        apiURL.searchParams.set('language', 'fr-FR');
        apiURL.searchParams.set('include_adult', false);

        try {
            console.debug(`Requête à l'API The Movie Database`, apiURL);
            const response = await fetch(apiURL, {
                method: 'GET',
            });

            if (response.ok) {
                const data = await response.json();
                console.debug(`Réponse de l'API The Movie Database`, data);

                return this.handleMovieResponse(data);
            } else {
                console.error(`Erreur HTTP: ${response.status} ${response.statusText}`);

                return this.getErrorMessage();
            }
        } catch (error) {
            console.error(`Erreur lors de la requête à l'API The Movie Database: ${error}`);

            return this.getErrorMessage();
        }
    }

    /**
     * Message d'erreur en cas de problème avec l'API.
     *
     * @returns {string} le message d'erreur.
     */
    static getErrorMessage() {
        return `Je suis désolé mais je n'ai pas réussi à effectuer la recherche. Veuillez réessayer ultérieurement.`;
    }

    /**
     * Formate les informations de la recherche.
     *
     * @param {Object} data les données de la recherche.
     *
     * @returns {string} le code HTML de la réponse.
     */
    static handleMovieResponse(data) {
        return `
            <p>J'ai trouvé ${data.total_results} résultats pour votre recherche :</p>
            <ul class="list-disc pl-6">
                ${data.results.map((movie) => `<li>${this.describeMovie(movie)}</li>`).join('')}
            </ul>
        `;
    }

    /**
     * Formate les informations d'un film.
     *
     * @param {Object} movieData les données du film.
     *
     * @returns {string} une phrase décrivant le film.
     */
    static describeMovie(movieData) {
        const { title, release_date, vote_average, vote_count } = movieData;
        const releaseYear = new Date(release_date).getFullYear();

        return `${title} (${releaseYear}) - Note : ${vote_average}/10 (${vote_count} votes).`;
    }
}
