export default class Github {
    /**
     * Retrouve les informations d'un utilisateur GitHub.
     *
     * https://docs.github.com/fr/rest/users/users?apiVersion=2022-11-28#get-a-user
     *
     * @param {string} username le nom d'utilisateur GitHub.
     *
     * @returns {Promise<string>} une phrase décrivant l'utilisateur GitHub.
     */
    static async getUser(username) {
        const apiURL = import.meta.env.VITE_GITHUB_USER_ENTRYPOINT;

        try {
            console.debug(`Requête à l'API Github`, `${apiURL}/${username}`);
            const response = await fetch(`${apiURL}/${username}`, {
                method: 'GET',
            });

            if (response.ok) {
                const data = await response.json();
                console.debug(`Réponse de l'API Github`, data);

                return this.describeGitHubUser(data);
            } else {
                console.error(`Erreur: ${response.status} ${response.statusText}`);

                return this.getErrorMessage();
            }
        } catch (error) {
            console.error(`Erreur lors de la requête à l'API Github: ${error}`);

            return this.getErrorMessage();
        }
    }

    /**
     * Message d'erreur en cas de problème avec l'API.
     *
     * @returns {string} le message d'erreur.
     */
    static getErrorMessage() {
        return `Je suis désolé mais je n'ai pas réussi à effectuer la recherche. Veuillez réessayer ultérieurement.`;
    }

    /**
     * Formate les informations d'un utilisateur GitHub.
     *
     * @param {Object} userData
     *
     * @returns {string} une phrase décrivant l'utilisateur GitHub.
     */
    static describeGitHubUser(userData) {
        const { login, html_url, public_repos, followers, following, created_at, updated_at, hireable } = userData;

        const hireableStatus = hireable ? 'disponible pour être embauché' : 'non disponible pour être embauché';
        const dateCreated = new Date(created_at).toLocaleDateString();
        const dateUpdated = new Date(updated_at).toLocaleDateString();

        return `L'utilisateur GitHub <a href="${html_url}" target="_blank" rel="noopener noreferrer" class="underline">${login}</a> possède ${public_repos} dépôts publics, ${followers} abonnés et suit ${following} utilisateurs. Le compte a été créé le ${dateCreated} et a été mis à jour le ${dateUpdated}. Cet utilisateur est actuellement ${hireableStatus}.`;
    }
}
