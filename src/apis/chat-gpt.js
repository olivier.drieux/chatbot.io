export default class ChatGPT {
    /**
     * Pose une question à ChatGPT et retourne la réponse.
     *
     * https://platform.openai.com/docs/guides/chat
     *
     * @param {string} question la question posée à ChatGPT.
     *
     * @returns {Promise<string>} la réponse de ChatGPT.
     */
    static async getResponse(question) {
        const apiURL = import.meta.env.VITE_CHATGPT_ENTRYPOINT;
        const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${import.meta.env.VITE_CHATGPT_APIKEY}`,
        };
        const body = JSON.stringify({
            model: 'gpt-3.5-turbo',
            messages: [{ role: 'user', content: question }],
        });

        try {
            console.debug(`Requête à l'API ChatGPT`, apiURL, headers, body);
            const response = await fetch(apiURL, {
                method: 'POST',
                headers: headers,
                body: body,
            });

            if (response.ok) {
                const data = await response.json();
                console.debug(`Réponse de l'API ChatGPT`, data);

                return data.choices[0].message.content;
            } else {
                console.error(`Erreur: ${response.status} ${response.statusText}`);

                return this.getErrorMessage();
            }
        } catch (error) {
            console.error(`Erreur lors de la requête à l'API ChatGPT: ${error}`);

            return this.getErrorMessage();
        }
    }

    /**
     * Message d'erreur en cas de problème avec l'API.
     *
     * @returns {string} le message d'erreur.
     */
    static getErrorMessage() {
        return `Je suis désolé mais je n'ai pas réussi à effectuer la demande. Veuillez réessayer ultérieurement.`;
    }
}
