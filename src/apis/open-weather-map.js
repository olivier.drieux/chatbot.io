export default class OpenWeatherMap {
    /**
     * Retrouve les informations de la météo d'une ville.
     *
     * https://openweathermap.org/current
     *
     * @param {string} city le nom de la ville.
     *
     * @returns {Promise<string>} une phrase décrivant la météo.
     */
    static async getWeather(city) {
        const apiURL = new URL(import.meta.env.VITE_OPENWEATHERMAP_WEATHER_ENTRYPOINT);
        apiURL.searchParams.set('q', city);
        apiURL.searchParams.set('appid', import.meta.env.VITE_OPENWEATHERMAP_APIKEY);
        apiURL.searchParams.set('lang', 'fr');
        apiURL.searchParams.set('units', 'metric');

        try {
            console.debug(`Requête à l'API OpenWeatherMap`, apiURL);
            const response = await fetch(apiURL, {
                method: 'GET',
            });

            if (response.ok) {
                const data = await response.json();
                console.debug(`Réponse de l'API OpenWeatherMap`, data);

                return this.describeWeather(data);
            } else {
                console.error(`Erreur HTTP: ${response.status} ${response.statusText}`);

                return this.getErrorMessage();
            }
        } catch (error) {
            console.error(`Erreur lors de la requête à l'API OpenWeatherMap: ${error}`);

            return this.getErrorMessage();
        }
    }

    /**
     * Message d'erreur en cas de problème avec l'API.
     *
     * @returns {string} le message d'erreur.
     */
    static getErrorMessage() {
        return `Je suis désolé mais je n'ai pas réussi à effectuer la recherche. Veuillez réessayer ultérieurement.`;
    }

    /**
     * Formate les informations de la météo.
     *
     * @param {Object} data
     *
     * @returns {string} une phrase décrivant la météo.
     */
    static describeWeather(data) {
        const cityName = data.name;
        const country = data.sys.country;
        const description = data.weather[0].description;
        const temperature = data.main.temp;
        const feelsLike = data.main.feels_like;
        const humidity = data.main.humidity;
        const windSpeed = data.wind.speed;

        return `La météo actuelle à ${cityName}, ${country} est: ${description}. La température est de ${temperature}°C et ressentie comme ${feelsLike}°C. L'humidité est de ${humidity}% avec une vitesse du vent de ${windSpeed} m/s.`;
    }
}
